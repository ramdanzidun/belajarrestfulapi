<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//belaajar API dengan ka vaisal reksi
Route::get("siswa/{id}","SiswaController@edit");
Route::post("siswa/search","SiswaController@search");
Route::resource("siswa","SiswaController");

//belajar restfulAPI dari video

Route::group(['prefix' => 'v1'], function(){
	Route::resource('meeting', 'MeetingController', [
		'except' => ['create','edit']
	]);

	Route::resource('meeting/registration', 'RegistrationController', [
		'only' => ['strore','destroy']
	]);

	Route::post('/user/register', [
		'uses' => 'AuthController@store'
	]);

	Route::post('/user/signin', [
		'uses' => 'AuthController@signin'
	]);
});