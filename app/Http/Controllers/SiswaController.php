<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswas;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = new Siswas;
        $result = $data->get();

        return \Response::json($result,200);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $data = new Siswas;
        $data->nis = $input['nis'];
        $data->nama = $input['nama'];
        $data->rombel = $input['rombel'];
        $data->rayon = $input['rayon'];
        $data->jk = $input['jk'];
        $data->agama = $input['agama'];
        $data->alamat = $input['alamat'];

        if (!$data->save()) {
            return \Response::json(['status'=>'Gagal'],500);
        }
            return \Response::json(['status'=>'Berhasil'],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Siswas::find($id);
        if (empty($data)) {
            return \Response::json(['status'=>'Data Tidak Ada'],500);
        }
        $data = $data->get();
            return \Response::json($data,200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $data = Siswas::find($id);
        $data->nama = $input['nama'];
        $data->rombel = $input['rombel'];
        $data->rayon = $input['rayon'];
        $data->jk = $input['jk'];
        $data->agama = $input['agama'];
        $data->alamat = $input['alamat'];

        if (!$data->save()) {
            return \Response::json(['status'=>'Gagal Update'],500);
        }
            return \Response::json(['status'=>'Berhasil Update'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Siswas::find($id);
        if (!$data->delete()) {
            return \Response::json(['status'=>'Gagal Hapus'],500);
        }
            return \Response::json(['status'=>'Berhasil Hapus'],200);

    }

    public function search(Request $request){
        $input = $request->all();
        $data = Siswas::where('nama',"LIKE","%".$input['nama']."%")->get();

        if ($data->isEmpty()) {
            return \Response::json(["status"=>"Data Tidak Ada"],500);
        }
            return \Response::json($data,200);
    }
}
