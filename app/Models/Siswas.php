<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswas extends Model
{
    protected $table = "siswa";
    public $timestaps = true;
    protected $primaryKey = "id";

    protected $fillable = [
    	'nis',
    	'nama',
    	'rombel',
    	'rayon',
    	'jk',
    	'agama',
    	'alamat'
    ];
}
